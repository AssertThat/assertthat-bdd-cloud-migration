import json
import requests

# Load config
with open('./config/global.json') as config_file:
    config = json.load(config_file)

jira_server_url = config['jira_server_url']
jira_server_username = config['jira_server_username']
jira_server_password = config['jira_server_password']

# Get projects with AssertThat enabled on Jira Server/DC
response = requests.get(
    jira_server_url + '/rest/assertthat/latest/client/report/projects?start=0&length=100',
    auth=(jira_server_username, jira_server_password))

if response.status_code != 200:
    print("[ERROR] Failed to call server get executions endpoint: " + response.text)

projects_stats = json.loads(response.text)
for p in projects_stats['projects']:
    p['jira_cloud_project_id'] = ''
    p['assertthat_access_key'] = ''
    p['assertthat_secret_key'] = ''

with open('./output/server-projects.json', 'w') as output_file:
    json.dump(projects_stats, output_file, indent=4)