import requests
from zipfile import ZipFile
from io import BytesIO
import json
import os
import re

# Load config
with open('./config/global.json') as config_file:
    config = json.load(config_file)

jira_server_url = config['jira_server_url']
jira_server_username = config['jira_server_username']
jira_server_password = config['jira_server_password']

asserttthat_host = config['asserttthat_host']

# Load server projects
with open('./output/server-projects.json') as server_projects_file:
    server_projects_config = json.load(server_projects_file)

# Download project config from Jira Server/DC
headers = {}
output_file = './output/projects/%s.json'
for p in server_projects_config['projects']:
    project_id = str(p['id'])
    try:
        response = requests.get(
            jira_server_url + "/rest/assertthat/latest/project/" + project_id + "/client/configuration",
            auth=(jira_server_username, jira_server_password), headers=headers)

        if response.status_code == 200:
            project_config = json.loads(response.text)
            os.makedirs(os.path.dirname(output_file % project_id), exist_ok=True)
            with open(output_file % project_id, 'w') as out_file:
                json.dump(project_config, out_file, indent=4)

        response.raise_for_status()

    except requests.exceptions.HTTPError as errh:
        response_content = response.content
        print("[ERROR] Failed to extract project config %s" % (response_content))

    except requests.exceptions.RequestException as err:
        print("[ERROR] Failed to extract project config", err)

input_file = './output/projects/%s.json'
for p in server_projects_config['projects']:
    project_id = str(p['id'])
    jira_cloud_project_id = p['jira_cloud_project_id']
    assertthat_access_key = p['assertthat_access_key']
    assertthat_secret_key = p['assertthat_secret_key']

    headers = {'content-type':'application/json'}
    try:
        with open(input_file % project_id) as fh:
            project_config_json = fh.read()
            response = requests.put(
                asserttthat_host + '/rest/api/1/project/' + jira_cloud_project_id + '/configuration',
                auth=(assertthat_access_key, assertthat_secret_key), headers=headers,
                data=project_config_json)

            if response.status_code == 200:
                print("[INFO] Uploaded project config file %s" % (input_file % project_id))

            response.raise_for_status()

    except requests.exceptions.HTTPError as errh:
        print("[ERROR] While uploading %s" % (input_file % project_id), errh)

    except requests.exceptions.RequestException as err:
        print("[ERROR] Failed to send request", err)
