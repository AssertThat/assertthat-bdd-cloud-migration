import json
import sys

import requests

# ====== CONFIG ========
# SERVER
# Load config
with open('./config/global.json') as config_file:
    config = json.load(config_file)

jira_server_url = config['jira_server_url']
jira_server_username = config['jira_server_username']
jira_server_password = config['jira_server_password']
asserttthat_host = config['asserttthat_host']

# CLOUD
jira_cloud_url = config['jira_cloud_url']
jira_cloud_username = config['jira_cloud_username']  # user email with browse users and groups global permissions
# https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-users/#api-rest-api-3-users-search-get

jira_cloud_api_token = config['jira_cloud_api_token']  # How to manage API tokens:
# https://support.atlassian.com/atlassian-account/docs/manage-api-tokens-for-your-atlassian-account/


with open('./output/server-projects.json') as server_projects_file:
    server_projects_config = json.load(server_projects_file)

# Add timezone shift. E.g. for Pacific Time (PST/PDT) set to -7 and for CET to 1
zone_shift = 0

# ====== CONFIG END ========

batch_size = 100
for p in server_projects_config['projects']:
    jira_server_project_id = str(p['id'])
    jira_cloud_project_id = p['jira_cloud_project_id']
    assertthat_access_key = p['assertthat_access_key']
    assertthat_secret_key = p['assertthat_secret_key']
    print("[INFO] Uploading executions for project " + jira_server_project_id)
    # Calculate number of scenarios/iterations in server
    response = requests.get(
        jira_server_url + '/rest/assertthat/latest/project/' + jira_server_project_id + '/client/scenarios/report/executions?start=0&length=1',
        auth=(jira_server_username, jira_server_password))

    if response.status_code != 200:
        print("[ERROR] Failed to call server get executions endpoint: " + response.text)

    total = json.loads(response.text)['total']

    iterations = round(total / batch_size + 0.5)
    start = 0

    usersStartAt = 0
    usersIterations = 10
    maxResults = 1000

    users_dict = {}

    for i in range(usersIterations):
        # Get users from could
        get_cloud_users_response = requests.get(
            jira_cloud_url + '/rest/api/3/users?includeInactive=true&startAt=' + str(
                usersStartAt) + '&maxResults=' + str(maxResults),
            auth=(jira_cloud_username, jira_cloud_api_token))
        if get_cloud_users_response.status_code != 200:
            print("[ERROR] Failed to call cloud get users endpoint: " + get_cloud_users_response.text)

        cloud_users = json.loads(get_cloud_users_response.text)
        if len(cloud_users) == 0:
            break
        else:
            # Get user email  - userid mapping from cloud
            for u in cloud_users:
                if 'displayName' in u:
                    users_dict[u['displayName'].lower()] = u['accountId']
            usersStartAt += maxResults

    # Get all scenarios from server and establish user mapping
    for i in range(iterations):
        get_executions_response = requests.get(
            jira_server_url + '/rest/assertthat/latest/project/' + jira_server_project_id + '/client/scenarios/report/executions?start=' + str(
                start) + '&length=' + str(batch_size),
            auth=(jira_server_username, jira_server_password))
        start += batch_size
        for s in json.loads(get_executions_response.text)['scenarios']:
            for e in s['executions']:
                if e['displayName'].lower() in users_dict:
                    e['userId'] = users_dict[e['displayName'].lower()]
                if zone_shift != 0:
                    e['timestamp'] = e['timestamp'] + (zone_shift * 60 * 60 * 1000)
            # Upload executions to cloud
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            try:
                upload_executions_response = requests.post(
                    url=asserttthat_host + '/rest/api/1/project/' + jira_cloud_project_id + '/scenarios/execution',
                    data=json.dumps(s), auth=(assertthat_access_key, assertthat_secret_key), headers=headers)
                if upload_executions_response.status_code != 200:
                    print("[ERROR] Status code: " + upload_executions_response.status_code)
                    print("[ERROR] Failed to import execution: " + json.dumps(s))
                    print("[ERROR] Failed to call cloud upload executions endpoint" + upload_executions_response.text + "")
            except:
                print("[ERROR] Failed to import execution: " + json.dumps(s))
                print("Unexpected error:", sys.exc_info()[0])
