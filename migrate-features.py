import requests
from zipfile import ZipFile
from io import BytesIO
import json
import os
import re

# Load config
with open('./config/global.json') as config_file:
    config = json.load(config_file)

jira_server_url = config['jira_server_url']
jira_server_username = config['jira_server_username']
jira_server_password = config['jira_server_password']

asserttthat_host = config['asserttthat_host']

# Load server projects
with open('./output/server-projects.json') as server_projects_file:
    server_projects_config = json.load(server_projects_file)

# Download featured from Jira Server/DC
headers = {}
payload = {'mode': 'both'}
output_folder = './output/features/%s'
for p in server_projects_config['projects']:
    if p['featuresCount'] == 0:
        continue
    project_id = str(p['id'])
    try:
        response = requests.get(jira_server_url + "/rest/assertthat/latest/project/" + project_id + "/client/features",
                                auth=(jira_server_username, jira_server_password), headers=headers, params=payload)

        if response.status_code == 200:
            print('Fetched.')
            print('Preparing to extract...')
            zip = ZipFile(BytesIO(response.content))
            infolist = zip.infolist()
            zip.extractall(path=output_folder % project_id)
            print('[INFO] Downloaded ' + str(infolist.__len__()) + ' feature files into "' + output_folder % project_id + '"')

        response.raise_for_status()

    except requests.exceptions.HTTPError as errh:
        response_content = response.content
        print("[ERROR] Failed to download features %s" % (response_content))

    except requests.exceptions.RequestException as err:
        print("[ERROR] Failed to download features", err)

# Upload features to Jira Cloud
for p in server_projects_config['projects']:
    if p['featuresCount'] == 0:
        continue
    project_id = str(p['id'])
    jira_cloud_project_id = p['jira_cloud_project_id']
    assertthat_access_key = p['assertthat_access_key']
    assertthat_secret_key = p['assertthat_secret_key']

    for subdir, dirs, files in os.walk(output_folder % project_id):
        print("[INFO] Uploading features for  %s " % project_id)
        for file in files:

            feature_file = os.path.join(subdir, file)
            upload_file = {'file': open(feature_file, 'rb')}

            headers = {}
            payload = {'override': 'true'}

            try:
                response = requests.post(asserttthat_host + '/rest/api/1/project/' + jira_cloud_project_id + '/feature',
                                         auth=(assertthat_access_key, assertthat_secret_key), headers=headers,
                                         params=payload,
                                         files=upload_file)
                response_content = json.loads(response.content)

                if response.status_code == 200:
                    print("[INFO] Uploaded feature file %s" % file)

                response.raise_for_status()

            except requests.exceptions.HTTPError as errh:
                print("[ERROR] While uploading %s" % file, errh)

            except requests.exceptions.RequestException as err:
                print("[ERROR] Failed to send request", err)
            except json.decoder.JSONDecodeError as jde:
                print("[ERROR] While uploading %s" % file)
                print("Response: %s" % response.content)
